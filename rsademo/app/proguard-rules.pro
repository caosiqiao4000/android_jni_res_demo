# ----------------------------------
#http://www.trinea.cn/android/proguard%E8%AF%AD%E6%B3%95%E5%8F%8A%E5%B8%B8%E7%94%A8proguard-cfg%E4%BB%A3%E7%A0%81%E6%AE%B5/
#  通过指定数量的优化能执行
#  -optimizationpasses n
# ----------------------------------
-optimizationpasses 5

# ----------------------------------
#   混淆时不会产生形形色色的类名
#   -dontusemixedcaseclassnames
# ----------------------------------
-dontusemixedcaseclassnames
# ----------------------------------
#      指定不去忽略非公共的库类
#  -dontskipnonpubliclibraryclasses
# ----------------------------------
-dontskipnonpubliclibraryclasses

# ----------------------------------
#       不预校验
#    -dontpreverify
# ----------------------------------
-dontpreverify

# ----------------------------------
#      输出生成信息
#       -verbose
# ----------------------------------
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*  #>>>优化
-dontwarn   #>>>不用输出警告
-dontskipnonpubliclibraryclassmembers #>>>指定不去忽略非公共的库类。
-dontskipnonpubliclibraryclasses      #>>>指定不去忽略包可见的库类的成员

#混淆时应用侵入式重载
-overloadaggressively

#优化时允许访问并修改有修饰符的类和类的成员
-allowaccessmodification
#确定统一的混淆类的成员名称来增加混淆
-useuniqueclassmembernames
-dontskipnonpubliclibraryclassmembers

########### --------- 保护类中的所有方法名 ------------
-keepclassmembers class * {
    public <methods>;
}

-keepclassmembers class **.R$* {
  public static <fields>;
}

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# ---------保护所有实体中的字段名称----------
-keepclassmembers class * implements java.io.Serializable {
    <fields>;
}

-keepclassmembers class * implements android.os.Parcel {
    <fields>;
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

# 不需要混淆的类
-keepattributes *Annotation*
-keepattributes JavascriptInterface
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class com.android.vending.billing.**
-keep public class com.example.android.trivialdrivesample.**{ *; }

#=================  上面是系统的
#=================  往下是自己的\引用的JAR
-keep class org.apache.avalon.framework.*{ *; }
-dontwarn org.apache.avalon.framework.**
-dontwarn org.apache.**
-dontwarn javax.activation.**
-dontwarn com.sun.activation.**
-dontwarn com.squareup.picasso.**
-dontwarn com.alipay.apmobilesecuritysdk.face.**
-dontwarn junit.**
-dontwarn java.lang.**

# Begin 这里添加你不需要混淆的类   一般自定义的bean类不混淆
-keep public class woying.icins.cn.Bean.**{*;}
#-keep public class woying.icins.cn.**{*;}
-keep public class woying.icins.cn.BuildConfig
-keep public class finance.nx.com.nacppproject.SecureUtil.**{*;}

#-keepclassmembers class * extends android.app.Activity {
#   public void *(android.view.View);
#}

#======================================== ANDROID SUPPORT 4
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class android.support.**{*;}
-dontwarn android.support.v4.**
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep public class * extends android.support.v4.**
-keep public class * extends android.app.Fragment

#-assumenosideeffects class android.util.Log {
#    public static *** d(...);
#    public static *** v(...);
#    public static *** i(...);
#    public static *** w(...);
#}

-dontwarn android.support.v13.**
-dontwarn com.squareup.picasso.OkHttpDownloader.**

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.  避免混淆泛型，这在JSON实体映射时非常重要，比如fastJson
-keepattributes Signature
# For using GSON @Expose annotation 保护代码中的Annotation不被混淆，这在JSON实体映射时非常重要，比如fastJson
-keepattributes *Annotation*

#保留Google GSON相关API:
##---------------Begin: proguard configuration for Gson && protobuf ----------
-keep class com.google.gson.** { *;}
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-dontwarn com.google.**
-keep class com.google.protobuf.** {*;}
##---------------End: proguard configuration for Gson  ----------


##-----------------jsoup------------------------- begin
-keep class org.jsoup.** { *; }
##-----------------jsoup------------------------- end
-keep class com.alipay.sdk.app.AuthTask{ public *;}
##---------------Begin : proguard configuration for alipay 支付宝  ----------
-dontwarn acom.alipay.apmobilesecuritysdk.face.**
-keep class com.squareup.picasso.OkHttpDownloader.** { *; }
-keep class com.alipay.apmobilesecuritysdk.face.APSecuritySdk.** { *; }
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
##---------------End: proguard configuration for alipay  ----------
##---------------Begin: proguard configuration for wexinpay   微信----------
-keep class com.tencent.mm.**{*;}
##---------------End: proguard configuration for  wexinpay  ----------
##---------------begin: proguard configuration for  友盟  ----------
##http://dev.umeng.com/social/android/android-update#14
-dontshrink
-dontoptimize
-dontwarn com.google.android.maps.**
-dontwarn android.webkit.WebView
-dontwarn com.umeng.**
-dontwarn com.tencent.weibo.sdk.**
-dontwarn com.facebook.**
-keep public class javax.**
-keep public class android.webkit.**
-dontwarn android.support.v4.**
-keep enum com.facebook.**
-keepattributes Exceptions,InnerClasses,Signature
-keepattributes *Annotation*
# 抛出异常时保留代码行号，在异常分析中可以方便定位
-keepattributes SourceFile,LineNumberTable

-keep public interface com.facebook.**
-keep public interface com.tencent.**
-keep public interface com.umeng.socialize.**
-keep public interface com.umeng.socialize.sensor.**
-keep public interface com.umeng.scrshot.**

-keep public class com.umeng.socialize.* {*;}


-keep class com.facebook.**
-keep class com.facebook.** { *; }
-keep class com.umeng.scrshot.**
-keep public class com.tencent.** {*;}
-keep class com.umeng.socialize.sensor.**
-keep class com.umeng.socialize.handler.**
-keep class com.umeng.socialize.handler.*
-keep class com.umeng.weixin.handler.**
-keep class com.umeng.weixin.handler.*
-keep class com.umeng.qq.handler.**
-keep class com.umeng.qq.handler.*
-keep class UMMoreHandler{*;}
-keep class com.tencent.mm.sdk.modelmsg.WXMediaMessage {*;}
-keep class com.tencent.mm.sdk.modelmsg.** implements com.tencent.mm.sdk.modelmsg.WXMediaMessage$IMediaObject {*;}
-keep class im.yixin.sdk.api.YXMessage {*;}
-keep class im.yixin.sdk.api.** implements im.yixin.sdk.api.YXMessage$YXMessageData{*;}
-keep class com.tencent.mm.sdk.** {
   *;
}
-keep class com.tencent.mm.opensdk.** {
   *;
}
-keep class com.tencent.wxop.** {
   *;
}
-keep class com.tencent.mm.sdk.** {
   *;
}
-dontwarn twitter4j.**
-keep class twitter4j.** { *; }

-keep class com.tencent.** {*;}
-dontwarn com.tencent.**
-keep class com.kakao.** {*;}
-dontwarn com.kakao.**
-keep public class com.umeng.com.umeng.soexample.R$*{
    public static final int *;
}
-keep public class com.linkedin.android.mobilesdk.R$*{
    public static final int *;
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.tencent.open.TDialog$*
-keep class com.tencent.open.TDialog$* {*;}
-keep class com.tencent.open.PKDialog
-keep class com.tencent.open.PKDialog {*;}
-keep class com.tencent.open.PKDialog$*
-keep class com.tencent.open.PKDialog$* {*;}
-keep class com.umeng.socialize.impl.ImageImpl {*;}
-keep class com.sina.** {*;}
-dontwarn com.sina.**
-keep class  com.alipay.share.sdk.** {
   *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep class com.linkedin.** { *; }
-keep class com.android.dingtalk.share.ddsharemodule.** { *; }
-keepattributes Signature
##---------------end: proguard configuration for  友盟 ----------
##---------------- https://github.com/Trinea/android-auto-scroll-view-pager
-keep class cn.trinea.android.** { *; }
-keepclassmembers class cn.trinea.android.** { *; }
-dontwarn cn.trinea.android.**

##------------------------------------ OkHttp  https://github.com/square/okhttp
-dontwarn okhttp3.**
-dontwarn java.nio.**
-dontwarn org.codehaus.**
#-keep class com.bumptech.glide.integration.okhttp3.OkHttpGlideModule
#-keep class com.qs.sdk.common.http.OkHttpGlideModule
-dontwarn okio.**
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
##------------------------------------ Jpush 极光推送 begin--------------
-dontoptimize
-dontpreverify

-dontwarn cn.jpush.**
-keep class cn.jpush.** { *; }

-dontwarn cn.jiguang.**
-keep class cn.jiguang.** { *; }

-keep class * extends cn.jpush.android.helpers.JPushMessageReceiver { *; }
##------------------------------------ Jpush 极光推送 end-------------------
##------------------------------------ M7kf 七陌客服系统 begin-------------------
-dontwarn javax.servlet.**
-dontwarn org.jboss.marshalling.**
-dontwarn org.osgi.**
-dontwarn java.net.**
-dontwarn java.nio.channels.**
-dontwarn org.apache.commons.**
-dontwarn org.apache.log4j.**
-dontwarn org.jboss.logging.**
-dontwarn org.slf4j.**
-dontwarn org.jboss.netty.**
-keep class org.jboss.netty.channel.socket.http.HttpTunnelingServlet {*;}
-keep class com.moor.imkf.** { *; }
-keep class com.j256.ormlite.** { *; }
##------------------------------------ M7kf 七陌客服系统 end-------------------
##------------------------------------  car_common end-------------------
-keep class com.icins.car.activity.** { *; }
-keep public class com.icins.car.model.** { *; }
#-keep public class mobile.**{*;}
##------------------------------------ car_common  end-------------------
##------------------------------------  RxJava RxAndroid begin
#使用注解需要添加
-keepattributes *Annotation*
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
##------------------------------------  RxJava RxAndroid end-------------------