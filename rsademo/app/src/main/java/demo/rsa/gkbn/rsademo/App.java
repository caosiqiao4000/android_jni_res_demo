package demo.rsa.gkbn.rsademo;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by xxb on 2017/8/4.
 */

public class App extends Application {

    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        LeakCanary.install(this);
    }

    public static App getInstance() {
        return app;
    }
}
