package demo.rsa.gkbn.rsademo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import finance.nx.com.nacppproject.SecureUtil;
import woying.icins.cn.R;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "AppCompatActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView tv = (TextView) findViewById(R.id.sample_text);

        Button rsa = (Button) findViewById(R.id.button);
        Button des = (Button) findViewById(R.id.button5);

        Button aes = (Button) findViewById(R.id.button6);
        Button md5 = (Button) findViewById(R.id.button7);
        Button base64 = (Button) findViewById(R.id.button8);


        rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                String s = new JniDemo().encryptReyByPrivateKey(App.getInstance(), "RSA加密测试-RSA加密测试");
//                String ss = SecureUtil.encryptReyByPrivateKey(App.getInstance(), "RSA加密测试-RSA加密测试");
//                String oldStr = "eyJtYWNoaW5lQ29kZSI6ImQ3MjAyNGNlMmRjNmYwM2UiLCJwYXNzV29yZCI6Ijk5NjkxMjRiZTcyMTM1ZGM3NDIzY2ZlYzAxM2UxMWFmIiwidXNlck5hbWUiOiIxODY4MDUzODgwNSIsInZlcnNpb24iOiIxLjAuOCJ91509604197195";
                String oldStr = "name";
                StringBuffer ss = new StringBuffer();
                if (oldStr.length() > 117) {
                    String[] array = {oldStr.substring(0, 117), oldStr.substring(117, oldStr.length())};
                    for (int i = 0; i < array.length; i++) {
                        ss.append(SecureUtil.encryptReyByPrivateKey(App.getInstance(), array[i]));
                    }
                } else {
                    System.out.println("woying onclick begin");
                    ss.append(SecureUtil.encryptReyByPrivateKey(App.getInstance(), oldStr));
                    System.out.println("woying onclick end");
                }

//                String decryptRSA = new JniDemo().decryptRSAByPublicKey(s);
//                tv.setText("加密结果：" + s + "\n" + "解密结果：" + decryptRSA);
                Log.i("aaaaaaaaaaaaaaaaaaaa", ss.toString());
                tv.setText("加密结果：" + ss.toString());
            }
        });

        des.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText(new JniDemo().decryptDES(new JniDemo().encryptDES("DES加密测试-DES加密测试-DES加密测试-DES加密测试")));
            }
        });

        aes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText(new JniDemo().decodeAES(new JniDemo().encodeAES("AES加密测试-AES加密测试-AES加密测试-AES加密测试")));
            }
        });


        md5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText(new JniDemo().MD5("MD5加密测试—MD5加密测试-MD5加密测试-MD5加密测试"));
            }
        });

        base64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv.setText(new JniDemo().decryptBase64(new JniDemo().encryptBase64("BASE64加密测试—BASE64加密测试-BASE64加密测试-BASE64加密测试")));
            }
        });

        final String date = getNowDateTime();
        System.out.println(date);
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getNowDateTime() {
        String now = formatDateTime(new java.util.Date(System.currentTimeMillis()));
        return now;
    }

    /**
     * 将时间格式化为 年-月-日 小时:分钟;秒 形式。例如 2001-01-01 19:06:01
     *
     * @param dDate 时间
     * @return String
     */
    public static String formatDateTime(java.util.Date dDate) {
        if (dDate == null) {
            return "";
        }
        DateFormat fdDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return fdDateFormat.format(dDate);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return "";
        }

    }


}
