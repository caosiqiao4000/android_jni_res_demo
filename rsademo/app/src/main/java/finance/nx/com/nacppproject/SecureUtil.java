package finance.nx.com.nacppproject;

import android.content.Context;
import android.util.Log;

/**
 * Created by csq on 2017/10/11 0011.
 */
public class SecureUtil {
    static {
        System.out.println("woying load library begin");
        System.loadLibrary("woying");
        System.out.println("woying load library end");
    }

    public native String MD5(String msg);

    public native String encodeAES(String msg);

    public native String decodeAES(String msg);


    public native String encryptDES(String msg);

    public native String decryptDES(String msg);

    // 公钥解密
    public native String decryptRSA(String msg);

    // 私钥解密
    public native String encryptRSA(String msg);

    //公钥验证
    public native String decryptRSAByPublicKey(String msg);

    //私钥加签
    public static native String encryptReyByPrivateKey(Context context, String msg);

    public native String encryptBase64(String msg);

    public native String decryptBase64(String msg);

    //======================================================
    public static String getAppVersion() {
        return "1.0";
    }

    public static String getChannel() {
        return "huawei";
    }

    public static void showToast(String tips) {
        Log.i("JniDemo",tips);
//        Toast.makeText(BaseApplication.getInstance(), tips, Toast.LENGTH_SHORT).show();
    }

//    public static String getSign(String data) {
//        return getSign(App.getInstance(), data);
//    }

//    native private static String getSign(Context context, String data);
}
