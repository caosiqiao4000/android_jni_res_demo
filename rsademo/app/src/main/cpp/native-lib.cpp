#include <jni.h>
#include <string>
#include "MyRSA.h"
#include <iostream>
#include "MyBASE64.h"
#include "My3DES.h"
#include "MyMD5.h"
#include "MyAES.h"

#include "check/Utils.h"
#include "check/EnvChecker.h"
#include "check/JavaClassesDef.h"
#include <sys/ptrace.h>

extern "C" {

__attribute ((visibility ("default")))
JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) //这是JNI_OnLoad的声明，必须按照这样的方式声明
{

    //防止动态调试
    ptrace(PTRACE_TRACEME, 0, 0, 0);

    JNIEnv *env = NULL;
    jint result = -1;
    if (vm->GetEnv((void **) &env, JNI_VERSION_1_4) != JNI_OK)
        return result;

    baseClasses.contextClass = (jclass) env->NewGlobalRef(
            env->FindClass("android/content/Context"));
    baseClasses.signatureClass = (jclass) env->NewGlobalRef(
            env->FindClass("android/content/pm/Signature"));
    baseClasses.packageManagerClass = (jclass) env->NewGlobalRef(
            env->FindClass("android/content/pm/PackageManager"));
    baseClasses.packageInfoClass = (jclass) env->NewGlobalRef(
            env->FindClass("android/content/pm/PackageInfo"));
    baseClasses.jniUtilClass = (jclass) env->NewGlobalRef(env->FindClass(SecureUtil_Clz));
    showToast(env, env->NewStringUTF("JNI_OnLoad one"));
    initAppEnv(env);

//    env->RegisterNatives(baseClasses.jniUtilClass, nMethods,
//                         sizeof(nMethods) / sizeof(nMethods[0]));

    gIsValid = false;

    logV("woying JNI OnLoad");

    return JNI_VERSION_1_4; //这里很重要，必须返回版本，否则加载会失败。
}
__attribute ((visibility ("default")))
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved) {
    //   __android_log_print(ANDROID_LOG_ERROR, "tag", "library was unload");
}


/**
 * base64加密
 */

__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_encryptBase64(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);


    std::string base64 = MyBASE64::base64_encodestring(msgC);

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(base64.c_str());
}

/**
 * base64 解密
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_decryptBase64(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);


    std::string base64 = MyBASE64::base64_decodestring(msgC);

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(base64.c_str());
}


/**
 * MD5加密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_MD5(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);


    std::string msgC;
    msgC.assign(msg);

    std::string f = MyMD5::encryptMD5(msgC);

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(f.c_str());
}


/**
 * AES加密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_encodeAES(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);


    std::string aes = MyAES::encodeAES("1234567812345678", msgC);//密码长度必须大于16 位


    std::string base64 = MyBASE64::base64_encodestring(aes);


    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(base64.c_str());
}


/**
 * AES解密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_decodeAES(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);


    int length;
    std::string base64 = MyBASE64::base64_decodestring(msgC);


    std::string aes = MyAES::decodeAES("1234567812345678", base64);

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(aes.c_str());
}

/**
 * DES加密算法
 */


__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_encryptDES(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);

    int length;
    std::string key = "12345678";
    std::string des = My3DES::encryptDES(msgC, key, &length);


    std::string base64 = MyBASE64::base64_encodestring(des);


    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(base64.c_str());
}


/**
 *
 * DES解密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_decryptDES(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);


    std::string msgC;
    msgC.assign(msg);

    std::string key = "12345678";
    int length;
    std::string base64 = MyBASE64::base64_decodestring(msgC);

    std::string des = My3DES::decryptDES(base64, key, base64.length());

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(des.c_str());
}


/**
 * RSA解密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_decryptRSA(JNIEnv *env, jobject instance, jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);

    std::string base64 = MyBASE64::base64_decodestring(msgC);

    std::string rsa = MyRSA::decryptRSA(base64);


    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(rsa.c_str());
}


/**
 * RSA  加密算法
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_encryptRSA(JNIEnv *env, jobject instance, jstring msg_) {


    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);

    std::string rsa = MyRSA::encryptRSA(msgC, NULL);


    std::string base64 = MyBASE64::base64_encodestring(rsa);

    env->ReleaseStringUTFChars(msg_, msg);


    return env->NewStringUTF(base64.c_str());

}

/**
 * RSA 公钥验证
 */
__attribute ((visibility ("default")))
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_decryptRSAByPublicKey(JNIEnv *env, jobject instance,
                                                         jstring msg_) {
    const char *msg = env->GetStringUTFChars(msg_, 0);

    std::string msgC;
    msgC.assign(msg);

    std::string base64 = MyBASE64::base64_decodestring(msgC);
    std::string rsa = MyRSA::decryptRSA(base64);

    env->ReleaseStringUTFChars(msg_, msg);

    return env->NewStringUTF(rsa.c_str());
}

/**
 * RSA 私钥加签
 */
JNIEXPORT jstring JNICALL
Java_finance_nx_com_nacppproject_SecureUtil_encryptReyByPrivateKey(JNIEnv *env, jobject instance,
                                                          jobject contextObject, jstring msg_) {
//    if (EnvChecker::isValid(env, instance)) {
    if (EnvChecker::isValid(env, contextObject)) {
        const char *msg = env->GetStringUTFChars(msg_, 0);

        std::string msgC;
        msgC.assign(msg);

        std::string rsa = MyRSA::encryptRSAByPrivate(msgC, NULL);
//        std::string base64 = MyBASE64::base64_encodestring(rsa);

        env->ReleaseStringUTFChars(msg_, msg);

        return env->NewStringUTF(rsa.c_str());
//        return env->NewStringUTF(base64.c_str());
    } else {
        showToast(env, env->NewStringUTF("woying 非法调用"));
        return msg_;
    }
}

}
