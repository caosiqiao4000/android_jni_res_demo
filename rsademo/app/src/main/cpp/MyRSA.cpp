//
// Created by gzbd on 2016/12/6.
//

#include "MyRSA.h"
#include <cstddef>
#include <stdlib.h>
#include "Log.h"


#include "openssllib/include/openssl/bio.h"

#include "openssllib/include/openssl/evp.h"
#include "openssllib/include/openssl/rsa.h"
#include "openssllib/include/openssl/pem.h"


#define  PUBLIC_EXPONENT   RSA_F4
#define   MODULUS "9c847aae8aa567d36af169dbed35f42f9568d137067b30a204476897020e7d88914d1c03a671c62be4a05fbd645bd358b2ff38ad2e5166003414eb7b155301d1f6cacaa54260681073e1c02947379614e6b6123e5b35af50dc675f1c673565979cc4acb967976e209bad50d24ab38b6822198644de43874e4fb92714f6fd677d"


#define PUBLICKEY "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwgKPvbsEqNbBKEuYqQMkCA792\nPddSfNdi5ANcxGneQ38tfeVl3hNGPVpsYMTQYqWeEv+NBufKBadpfBm6HZuOm5nu\nLfSZPPjrM60XBPU5tvg7LJEsS0Uh3hv1pKk8TJVdcfyak56CyMNXXu2r7lXL80GR\n7aXgB+lfj3zfpJIMlQIDAQAB\n-----END PUBLIC KEY-----\n"

#define PRIVATE_KEY  "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALS1/WKvoUTM7yHRo2VVlGsHRyWaWMrxF57Lo9gMuq5JSjeo1OxXd1PK8Ho/6emqeDCX8UcZmP/XBgpMpmlMrqrFqu5+E81AZk9Az6p8v+BZvgs1Lfap1zJ8l/9IVM6npNK+jQh+Fxsp4ObTlJG8cUVfkKB64cHqQ12SbBx5CCXjAgMBAAECgYA6Jz6tXVuGanSh63ari4jBkIwCu9LLLB6vyVSmmCITKB6sHra2f8c0vd/5V0wNGACpkpvgh4pG+olFEzmSNU0XuSnNxJc+D4sAOVxdq+lNVvijbuOhNzIHkW2KVNWj000ONz/XkSUHuhBP1+TurtlT9ueTMsHvcsAX2bYSzfL2IQJBANeh2b3uTVJgV+KJi8+xXwMhqq033rwzmOuBDkJHq4i06uDDQ5jwgaF6pYZG3YKHrPzetor7I5EQR7wcvfJ34usCQQDWionbivgjtgA/zJnzZjoO2TwABaEhoAWbB5n2S8N7kS7fY8WciAMsMfzviWTX6A5R5qRa/y8tGA7MmaZ7bFrpAkEApyTeE0tUxDXJ6d+UcDmc3GCMXBiinl+geWxgESBc5mrWZcL18ub6lKDtDViA+10PyjMYbEKfHyLvd/EPi5NCYQJABPCZrgFsJz5YkR9/5/fRnGfqFsIKbMDHgENTizVBYgX8w04Dzc/f6tOX1Fggh0wjkEU0QZggmPQ12wYqrCrZiQJAKdIHSwomTifLllca/FzuZs/rqgefsJM33+84By15cpeDisaN0rpwd5BQTlqb0g0GN5+J6HuccrZrFVdTDnEN0w=="


#define  PADDING   RSA_PKCS1_PADDING          //填充方式




std::string MyRSA::decryptRSA(const std::string &data) {


    int ret, flen;
    BIO *bio = NULL;
    RSA *r = NULL;

    if ((bio = BIO_new_mem_buf((void *) PRIVATE_KEY, -1)) == NULL)       //从字符串读取RSA公钥
    {
        LOGE("BIO_new_mem_buf failed!\n");
    }

    r = PEM_read_bio_RSAPrivateKey(bio, NULL, NULL, NULL);


    flen = RSA_size(r);


    static std::string gkbn;
    gkbn.clear();


    unsigned char *dst = NULL;


    dst = (unsigned char *) malloc(flen+1);

    bzero(dst, flen);


    int state = RSA_private_decrypt(data.length(), (unsigned char *) data.c_str(), dst, r,
                                    RSA_PKCS1_PADDING);//RSA_NO_PADDING //RSA_PKCS1_PADDING

    if (state < 0) {
        LOGE("RSA 解密失败");
    }

    gkbn.assign((char *) dst, state);//防止 尾部0 被截断


    BIO_free_all(bio);


    free(dst);

   // CRYPTO_cleanup_all_ex_data(); //清除管理CRYPTO_EX_DATA的全局hash表中的数据，避免内存泄漏
    dst=NULL;
    r=NULL;
    bio=NULL;

    return gkbn;
}


std::string MyRSA::encryptRSA(const std::string &data, int *lenreturn) {


    int ret, flen;
    BIO *bio = NULL;
    RSA *r = NULL;

    if ((bio = BIO_new_mem_buf((void *) PUBLICKEY, -1)) == NULL)       //从字符串读取RSA公钥
    {
        LOGE("BIO_new_mem_buf failed!\n");
    }

    r = PEM_read_bio_RSA_PUBKEY(bio, NULL, NULL, NULL);

    flen = RSA_size(r);

//    if (PADDING == RSA_PKCS1_PADDING || PADDING == RSA_SSLV23_PADDING) {
//        flen -= 11;
//    }
    lenreturn = &flen;

    static std::string gkbn;
    gkbn.clear();

    char *dst = (char *) malloc(flen+1);
    bzero(dst, flen);

    int status = RSA_public_encrypt(data.length(), (unsigned char *) data.c_str(),
                                    (unsigned char *) dst, r, RSA_PKCS1_PADDING);

    if (status < 0) {

        LOGE("RSA 加密失败");

    }



    gkbn.assign((char *) dst, status);


    RSA_free(r);
    BIO_free_all(bio);

    free(dst);
    dst=NULL;
    r=NULL;
    bio=NULL;

    //CRYPTO_cleanup_all_ex_data(); //清除管理CRYPTO_EX_DATA的全局hash表中的数据，避免内存泄漏

    return gkbn;

}

std::string MyRSA::encryptRSAByPrivate(const std::string &data, int *lenreturn) {
//    int ret, flen;
//    BIO *bio = NULL;
//    RSA *r = NULL;

//    if ((bio = BIO_new_mem_buf((void *) PRIVATE_KEY, -1)) == NULL)       //从字符串读取RSA公钥
//    {
//        LOGE("BIO_new_mem_buf failed!\n");
//    }
//
//    r = PEM_read_bio_RSAPrivateKey(bio, NULL, NULL, NULL);
//
//    flen = RSA_size(r);
//一直未使用的
//    if (PADDING == RSA_PKCS1_PADDING || PADDING == RSA_SSLV23_PADDING) {
//        flen -= 11;
//    }
//    lenreturn = &flen;
//
//    static std::string gkbn;
//    gkbn.clear();
//
//    char *dst = (char *) malloc(flen+1);
//    bzero(dst, flen);
//
//    int status = RSA_private_encrypt(data.length(), (unsigned char *) data.c_str(),
//                                    (unsigned char *) dst, r, RSA_PKCS1_PADDING);
//
//    if (status < 0) {
//        LOGE("RSA 加密失败");
//    }
//
//    gkbn.assign((char *) dst, status);
//
//    RSA_free(r);
//    BIO_free_all(bio);
//
//    free(dst);
//    dst=NULL;
//    r=NULL;
//    bio=NULL;

    //CRYPTO_cleanup_all_ex_data(); //清除管理CRYPTO_EX_DATA的全局hash表中的数据，避免内存泄漏

//    return gkbn;
    return PRIVATE_KEY;
}

std::string MyRSA::decryptRSAByPublic(const std::string &data) {
    return std::string();
}


